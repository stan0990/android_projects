package com.example.walter360.proyecto001;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText et1, et2;
    private TextView tv3;
    private RadioButton sum, rest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText)findViewById(R.id.editText1);
        et2 = (EditText)findViewById(R.id.editText2);
        tv3 = (TextView)findViewById(R.id.textView3);
        sum = (RadioButton)findViewById(R.id.sum);
        rest = (RadioButton)findViewById(R.id.rest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sumarElementos(View view){
        String valor1=et1.getText().toString();
        String valor2=et2.getText().toString();
        int nrol1 = Integer.parseInt(valor1);
        int nrol2 = Integer.parseInt(valor2);
        String resultado=null;
        if(sum.isChecked()==true)
        {
            int suma = nrol1 + nrol2;
            resultado = String.valueOf(suma);
        }
        else if(rest.isChecked()==true)
        {
            int resta = nrol1 - nrol2;
            resultado = String.valueOf(resta);
        }

        Toast notification = Toast.makeText(this, "El resultado es: "+ resultado, Toast.LENGTH_LONG);
        notification.show();
    }
}
