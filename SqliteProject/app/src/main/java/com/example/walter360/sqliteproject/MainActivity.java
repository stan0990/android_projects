package com.example.walter360.sqliteproject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText id, name, description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id=(EditText)findViewById(R.id.txId);
        name=(EditText)findViewById(R.id.txFullName);
        description=(EditText)findViewById(R.id.txDescription);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveData(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "joven360", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String cod = id.getText().toString();
        String full_name = name.getText().toString();
        String descrip = description.getText().toString();
        ContentValues registro = new ContentValues();
        registro.put("candidate_id", cod);
        registro.put("full_name", full_name);
        registro.put("description", descrip);
        db.insert("candidates", null, registro);
        db.close();
        id.setText("");
        name.setText("");
        description.setText("");
        Toast.makeText(this, "Se cargaron los datos del candidato", Toast.LENGTH_SHORT).show();
    }

    public void searchCode(View v){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "joven360", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String cod = id.getText().toString();
        Cursor fila = db.rawQuery("select description, full_name from candidates where candidate_id=" + cod, null);
        if(fila.moveToFirst()){
            name.setText(fila.getString(1));
            description.setText(fila.getString(0));
        }else{
            Toast.makeText(this, "No existe un candidato con dicho código", Toast.LENGTH_SHORT).show();
            db.close();
        }
    }

    public  void deleteData(View v){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "joven360", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String cod = id.getText().toString();
        int cant= db.delete("candidates", "candidate_id = " + cod, null);
        db.close();
        id.setText("");
        name.setText("");
        description.setText("");
        if(cant==1){
            Toast.makeText(this, "Se borró el candidato con dicho código", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "No existe un candidato con dicho código", Toast.LENGTH_SHORT).show();
        }
    }

    public void editData(View v){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "joven360", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String cod = id.getText().toString();
        String full_name = name.getText().toString();
        String descrip = description.getText().toString();
        ContentValues registro = new ContentValues();
        registro.put("candidate_id", cod);
        registro.put("full_name", full_name);
        registro.put("description", descrip);
        int cant = db.update("candidates", registro, "candidate_id = "+ cod, null);
        db.close();
        if(cant==1){
            Toast.makeText(this, "Se modificaron los datos", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "No existe un candidato con dicho código", Toast.LENGTH_SHORT).show();
        }
    }
}
