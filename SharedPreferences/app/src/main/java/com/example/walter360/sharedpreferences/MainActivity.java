package com.example.walter360.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txName, txEmail, txBirthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txName = (EditText)findViewById(R.id.txPersonName);
        txEmail = (EditText)findViewById(R.id.txEmail);
        txBirthday = (EditText)findViewById(R.id.txBirthday);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveData(View v){
        String name= txName.getText().toString();
        String email= txEmail.getText().toString();
        String birthday= txBirthday.getText().toString();

        SharedPreferences preferences = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString(name, email);
        editor.commit();
        Toast.makeText(this, "Datos grabados correctamente", Toast.LENGTH_LONG).show();
    }

    public void recoveryData(View v){
        String name= txName.getText().toString();
        SharedPreferences preferences = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String d=preferences.getString(name, "");
        if (d.length()==0) {
            Toast.makeText(this,"No existe dicho nombre en la agenda",Toast.LENGTH_LONG).show();
        }
        else {
            txEmail.setText(d);
        }
    }
}
